package son.nv.coroutines_example

class User {

     var name : String = ""
        get() = field
        set(value) {
            field = value
        }

     var phone : String = ""
        get() = field
        set(value) {
            field = value
        }

     var address : String = ""
        get() = field
        set(value) {
            field = value
        }

    constructor() {
    }

    constructor(name: String, phone: String, address: String) {
        this.name = name
        this.phone = phone
        this.address = address
    }

}