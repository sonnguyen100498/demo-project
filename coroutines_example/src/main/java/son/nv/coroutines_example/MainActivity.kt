package son.nv.coroutines_example

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext


class MainActivity : AppCompatActivity(), CoroutineScope {
    private lateinit var textView1 : TextView
    private lateinit var textView2 : TextView

    private lateinit var job: Job
    private lateinit var handler : CoroutineExceptionHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textView1 = findViewById(R.id.textView)
        textView2 = findViewById(R.id.textView2)

        job = Job()
        handler = CoroutineExceptionHandler { _, exception ->
            Log.d("MainActivity", "$exception handled !")
        }

    //  show user chạy trên main Thread
    //  launch thực hiện và không có giá trị trả về
    //  GlobalScope.launch : khi scope la toan ung dung ko phai la activity

        fetchUser1()

    }

    //  fetchUser chạy trên background io thread
    private suspend fun fetchUser () : User {
        // make network call
        // return user
        lateinit var user : User
        //  async thực hiện và trả về giá trị
        //  await() là cách ứng dụng đợi và nhận kết quả trả về từ coroutine được khởi động bằng async

        GlobalScope.async(Dispatchers.IO + handler)  {
            delay(2000)
            user = User("user name 1", "03484847", "viet nam")
        }.await()
        return user
    }


    // thuc hien song song tra ve gia tri
    private fun fetchUser1() {
        val startTime =   System.nanoTime()
        GlobalScope.launch(Dispatchers.Main) {
            val userOne = async(Dispatchers.IO) { fetchUser() }
            val userTwo = async(Dispatchers.IO) { fetchUser() }
            showUser(userOne.await(), userTwo.await()) // back on UI thread

            Log.i("Measure", "Time fetchUser1 ${System.nanoTime() - startTime }")
        }

    }

    // thuc hien lan luot tra ve gia tri
    private fun fetchUser2() {
        val startTime =   System.nanoTime()
        GlobalScope.launch(Dispatchers.Main) {
            val userOne = withContext(Dispatchers.IO) { fetchUser() }
            val userTwo = withContext(Dispatchers.IO) { fetchUser() }
            showUser(userOne, userTwo) // back on UI thread

            Log.i("Measure", "Time fetchUser2 ${System.nanoTime() - startTime }")
        }

    }

    suspend fun fetchAndShowUser () {
        val user = fetchUser()
        showUser(user, user)
    }

    private fun showUser (user1: User, user2: User) {
        textView1.text = user1.name
        textView2.text = user2.name
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job + handler


    override fun onDestroy() {
        job.cancel() // cancel the Job
        super.onDestroy()
    }
}