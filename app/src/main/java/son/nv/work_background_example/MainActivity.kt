package son.nv.work_background_example

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.work.*
import com.example.worker_examlpe.workerClass
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {
    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // set rang buoc

        val constraints = Constraints.Builder()
//            .setRequiresDeviceIdle(true)
//            .setRequiresCharging(true)
//            .setRequiredNetworkType(NetworkType.CONNECTED)
//            .setRequiresBatteryNotLow(true)
//            .setRequiresStorageNotLow(true)
            .build()

        // set du lieu dau vao
        val dataInput = Data.Builder()
            .putInt("a", 8)
            .putInt("b", 3)
            .build()

        val workRequestOneTime1 = OneTimeWorkRequest.Builder(workerClass::class.java)
//            .setInitialDelay(10, TimeUnit.MINUTES) // run after 10 minutes
            .setConstraints(constraints)
            .addTag("workRequestOneTime1")
            .setInputData(dataInput)
            .build()

        val workRequestOneTime2 = OneTimeWorkRequest.Builder(workerClass::class.java)
//            .setInitialDelay(10, TimeUnit.MINUTES) // run after 10 minutes
            .setConstraints(constraints)
            .setInputData(dataInput)
            .addTag("workRequestOneTime2")
            .build()

        val workRequestPeriodic = PeriodicWorkRequest
            .Builder(workerClass::class.java, 1 , TimeUnit.SECONDS)
//            .setConstraints(constraints)
            .build()

        val workManager : WorkManager = WorkManager.getInstance(this)
//         workManager.enqueue(workRequestPeriodic)

//         thuc thi lan luot cac task
//         workManager.beginWith(workRequestOneTime1).then(workRequestOneTime2).enqueue()

        // thuc thi song song cac task
        workManager.beginWith(listOf(workRequestOneTime1, workRequestOneTime2)).enqueue()

        workManager.getWorkInfoByIdLiveData(workRequestOneTime1.id)
            .observe(this) { workInfo ->
                if (workInfo != null && workInfo.state == WorkInfo.State.SUCCEEDED) {
                    val data: Int = workInfo.outputData.getInt("data", 0)
                    Log.d("MainActivity1", "WorkInfo received: state: " + workInfo.state + " ; output data - $data")
                } else {
                    Log.d("MainActivity1", "WorkInfo fail")
                }
            }

        workManager.getWorkInfoByIdLiveData(workRequestOneTime1.id)
            .observe(this) { workInfo ->
                if (workInfo != null && workInfo.state == WorkInfo.State.SUCCEEDED) {
                    val data: Int = workInfo.outputData.getInt("data", 0)
                    Log.d("MainActivity2", "WorkInfo received: state: " + workInfo.state + " ; output data - $data")
                } else {
                    Log.d("MainActivity2", "WorkInfo fail")
                }
            }
    }

    override fun onDestroy() {
        super.onDestroy()
        WorkManager.getInstance(this).cancelAllWorkByTag("workRequestOneTime1")
    }

}