package com.example.worker_examlpe

import android.content.Context
import android.util.Log
import androidx.work.Data
import androidx.work.Worker
import androidx.work.WorkerParameters

class workerClass (context: Context, workerParameters: WorkerParameters) : Worker (context, workerParameters)  {

    override fun doWork(): Result {
        var a : Int = inputData.getInt("a", 0)
        var b : Int = inputData.getInt("b", 0)
        var valueData : Int = addNumber(a,b)

        val data = Data.Builder()
            .putInt("data", valueData)
            .build()

        return Result.success(data)
    }

    private fun addNumber (a : Int, b : Int) : Int{
        Log.i("TAG", "addNumber")
        return a + b
    }
}